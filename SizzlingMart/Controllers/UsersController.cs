﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SizzlingMart.Models.ViewModel;
using SizzlingMart.Models.User;
using System.Data.Entity;
using SizzlingMart.Models.Product;
using SizzlingMart.Models;
using SizzlingMart.Models.Order;

namespace SizzlingMart.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        [HttpGet]
        public ActionResult Login()
        {
            HttpCookie usercookie = Request.Cookies[WebUtil.USER_COOKIE];
            if (usercookie != null)
            {
                string[] data = usercookie.Value.Split(',');
                User user = new UserHandler().GetUser(data[0], data[1]);
                if (user != null)
                {
                    Session.Add(WebUtil.CURRENT_USER, user);
                    usercookie.Expires = DateTime.Today.AddDays(7);
                    Response.SetCookie(usercookie);
                    if (user.IsInRole(WebUtil.AdminRole))
                    {
                        return RedirectToAction("Admin", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            ViewBag.ReturnUrl = Request.QueryString["returl"];
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            string temp = Request.QueryString["rurl"];
            string encryptedpassword = PasswordEncryptDecrypt.Encrypt(model.Password1);
            User user = new UserHandler().GetUser(model.Email1, encryptedpassword);
            if (user != null)
            {
                Session.Add(WebUtil.CURRENT_USER, user);
                if (Request.Browser.Cookies)
                {
                    if (model.RememberMe1)
                    {
                        HttpCookie usercoookie = new HttpCookie(WebUtil.USER_COOKIE);
                        usercoookie.Expires = DateTime.Today.AddDays(7);
                        usercoookie.Value = $"{model.Email1},{model.Password1}";
                        Response.SetCookie(usercoookie);
                    }
                }
                if (user.IsInRole(WebUtil.AdminRole))
                {
                    if (!string.IsNullOrEmpty(temp))
                    {
                        string[] parts = temp.Split('/');
                        return RedirectToAction(parts[1], parts[0]);
                    }
                    return RedirectToAction("Admin", "Home");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
                
            }
            if (!string.IsNullOrEmpty(temp))
            {
                return RedirectToAction("Login", "Users", new { returl = "products/admin" });
            }

            int attempts = Convert.ToInt32(Request["Attempts"]);
            TempData.Add("Attempts", ++attempts);
            if (attempts < 3)
            {
                TempData["error"] = "Warning! Provided Email ID or Password is incorrect. Try Again!";
                return RedirectToAction("Login", "Users");
            }
            return RedirectToAction("ForgotPassword", "Users");
        }

        public ActionResult Registered()
        {
            RegisterModel model = new RegisterModel();
            return View(model);
        }

        public ActionResult ForgotPassword()
        {
            RegisterModel model = new RegisterModel();
            return View(model);
        }

        public ActionResult MyAccount()
        {
            if (Session[WebUtil.CURRENT_USER] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpGet]
        public ActionResult MyOrders(int id)
        {
            if (Session[WebUtil.CURRENT_USER] != null)
            {
                List<OrderInfo> model = new OrdersHandler().MyOrders(id);
                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult OrderInfo(int id)
        {
            using (DAL dal = new DAL())
            {
                List<OrderInfo> model = (from o in dal.OrderInfoes
                                    .Include("OrderDetails")
                                     where o.Id == id select o).ToList();
                return PartialView("~/Views/Shared/_ModalOrder.cshtml", model);
            }
        }

        public ActionResult ChangePassword()
        {
            if (Session[WebUtil.CURRENT_USER] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (Session[WebUtil.CURRENT_USER] != null && ModelState.IsValid)
            {
                User user = ((User)Session[WebUtil.CURRENT_USER]);
                using (DAL dal = new DAL())
                {
                    user.Password = PasswordEncryptDecrypt.Encrypt(model.ChangedPassword);
                    dal.Entry(user).State = EntityState.Modified;
                    dal.SaveChanges();
                    return RedirectToAction("MyAccount", "Users");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }

        [HttpGet]
        public ActionResult EditAccount()
        {
            if (Session[WebUtil.CURRENT_USER] != null)
            {
                User user = ((User)Session[WebUtil.CURRENT_USER]);
                EditModel model = new EditModel();
                model.FirstName = user.FirstName;
                model.LastName = user.LastName;
                model.Contact = user.ContactNumber;
                model.Address = user.Address.StreetAddress;
                model.Email = user.Email;
                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }

        [HttpPost]
        public ActionResult EditAccount(EditModel model)
        {
            if (Session[WebUtil.CURRENT_USER] != null)
            {
                User user = ((User)Session[WebUtil.CURRENT_USER]);
                using (DAL dal = new DAL())
                {
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.ContactNumber = model.Contact;
                    user.Address.StreetAddress = model.Address;
                    dal.Entry(user).State = EntityState.Modified;
                    dal.Entry(user.Address).State = EntityState.Modified;
                    dal.SaveChanges();
                    return RedirectToAction("MyAccount", "Users");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }


        public ActionResult Register()
        {
            ViewBag.Countries = ProductHelper.ToSelectItemList(ProductHandler.GetCountries());
            ViewBag.Cities = ProductHelper.ToSelectItemList(ProductHandler.GetCities());
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (model.PrivacyPolicy == true)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        DAL dal = new DAL();
                        User usr = new User();
                        usr.FirstName = model.FirstName;
                        usr.LastName = model.LastName;
                        usr.Email = model.Email;
                        usr.Password = PasswordEncryptDecrypt.Encrypt(model.RegPassword);
                        usr.ContactNumber = model.Contact;
                        usr.Address = new Address { StreetAddress = model.Address };
                        usr.Address.City = new City { Id = Convert.ToInt32(model.City) };
                        usr.Address.City.Country = new Country { Id = Convert.ToInt32(model.Country) };
                        usr.CreatedDate = DateTime.Now;
                        usr.Role = new Role { Id = 2 };
                        usr.IsActive = true;

                        dal.Entry(usr.Address.City).State = EntityState.Unchanged;
                        dal.Entry(usr.Address.City.Country).State = EntityState.Unchanged;
                        dal.Entry(usr.Role).State = EntityState.Unchanged;
                        dal.Users.Add(usr);
                        dal.SaveChanges();
                        Session.Add(WebUtil.CURRENT_USER, usr);
                    }
                    catch (Exception)
                    {

                        TempData["error"] = "Sorry for the inconvenience, Please try to register yourself later...";
                        return RedirectToAction("Register", "Users");
                    }

                    TempData["SuccessfullySignedUp"] = "Regisered Successfully. Please 'SignIn' to continue...!";
                    return RedirectToAction("Registered", "Users");
                }
                else
                {
                    TempData["error"] = "Sorry for the inconvenience, Please try to register yourself later...";
                    return RedirectToAction("Register", "Users");
                }
            }
            else
            {
                TempData["error"] = " Warning: You must read and agree to our Privacy Policy, before to get registered.";
                return RedirectToAction("Register", "Users", model);
            }
        }

        [HttpGet]
        public string CheckEmailExists(string Email)
        {
            var EmailExists = "";
            using (DAL dal = new DAL())
            {
                var user = dal.Users.FirstOrDefault((n) => n.Email == Email);
                if (user == null)
                {
                    EmailExists = "No";
                }
                else
                {
                    EmailExists = "Yes";
                }
                return EmailExists;

            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Session.Abandon();
            HttpCookie c = Request.Cookies[WebUtil.USER_COOKIE];
            if (c != null)
            {
                c.Expires = DateTime.Now;
                Response.SetCookie(c);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}