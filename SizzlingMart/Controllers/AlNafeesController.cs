﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SizzlingMart.Models.Product;
using SizzlingMart.Models.ViewModel;

namespace SizzlingMart.Controllers
{
    public class AlNafeesController : Controller
    {
        // GET: AlNafees
        public ActionResult Index()
        {
            ViewBag.Perfumes = ProductHelper.ToMinDetailsList(ProductHandler.GetLatestProductsByCategory(30, 8));
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            return View();
        }
    }
}