﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SizzlingMart.Models.ViewModel;
using SizzlingMart.Models.Product;
using PagedList;

namespace SizzlingMart.Controllers
{
    public class PrinceBoutiqueController : Controller
    {
        // GET: PrinceBoutique
        [HttpGet]
        public ActionResult Index(int page = 1, int pageSize = 12)
        {
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> princeboutique = ProductHelper.ToMinDetailsList(ProductHandler.GetProductsBySeller(1));
            PagedList<MinDetails> model = new PagedList<MinDetails>(princeboutique, page, pageSize);
            return View(model);
        }
    }
}