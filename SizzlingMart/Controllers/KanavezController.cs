﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SizzlingMart.Models.Product;
using SizzlingMart.Models.ViewModel;

namespace SizzlingMart.Controllers
{
    public class KanavezController : Controller
    {
        // GET: Kanavez
        public ActionResult Index()
        {
            ViewBag.Kanavez = ProductHelper.ToMinDetailsList(ProductHandler.GetLatestProductsByCategory(9, 7));
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            return View();
        }
    }
}