﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SizzlingMart.Models.Product;
using SizzlingMart.Models.ViewModel;
using SizzlingMart.Models;
using SizzlingMart.Models.User;
using PagedList;
using System.Data.Entity;

namespace SizzlingMart.Controllers
{
    public class ProductsController : Controller
    {
        
        [HttpGet]
        public ActionResult Add()
        {
            ViewBag.Categories = ProductHelper.ToSelectItemList(ProductHandler.GetCategories());
            ViewBag.Sellers = ProductHelper.ToSelectItemListForSellers(ProductHandler.GetSellers());
            ViewBag.Brands = ProductHelper.ToSelectItemList(ProductHandler.GetBrands());
            ViewBag.Wears = ProductHelper.ToSelectItemList(ProductHandler.GetWears());
            ViewBag.AvailableColors = ProductHelper.ToSelectItemList(ProductHandler.GetAvailableColors());
            return View();
        }

        [HttpGet]
        public ActionResult ProductsBySubCategory(int id, int page = 1, int pageSize = 12)
        {
            TempData["id"] = id;
            ViewBag.SC_Name = "ProductsBySubCategory";
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> productbysubcategory = ProductHelper.ToMinDetailsList(ProductHandler.GetProductsBySubCategoryOriginal(id));
            PagedList<MinDetails> model = new PagedList<MinDetails>(productbysubcategory, page, pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult MakeUp(int page = 1, int pageSize = 12)
        {
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> makeup = ProductHelper.ToMinDetailsList(ProductHandler.GetProductsByCategory(2));
            PagedList<MinDetails> model = new PagedList<MinDetails>(makeup, page, pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult MakeUpSC(int id, int page = 1, int pageSize = 12)
        {
            TempData["id"] = id;
            ViewBag.SC_Name = "MakeUpSC";
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> makeupsc = ProductHelper.ToMinDetailsList(ProductHandler.GetProductsBySubCategory(id));
            PagedList<MinDetails> model = new PagedList<MinDetails>(makeupsc, page, pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult SkinCare(int page = 1, int pageSize = 12)
        {
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> skincare = ProductHelper.ToMinDetailsList(ProductHandler.GetProductsByCategory(4));
            PagedList<MinDetails> model = new PagedList<MinDetails>(skincare, page, pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult SkinCareSC(int id, int page = 1, int pageSize = 12)
        {
            TempData["id"] = id;
            ViewBag.SC_Name = "SkinCareSC";
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> skincaresc = ProductHelper.ToMinDetailsList(ProductHandler.GetProductsBySubCategory(id));
            PagedList<MinDetails> model = new PagedList<MinDetails>(skincaresc, page, pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult Clothing(int page = 1, int pageSize = 12)
        {
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> clothing = ProductHelper.ToMinDetailsList(ProductHandler.GetProductsByCategory(1));
            PagedList<MinDetails> model = new PagedList<MinDetails>(clothing, page, pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult ClothingSC(int id, int page = 1, int pageSize = 12)
        {
            TempData["id"] = id;
            ViewBag.SC_Name = "ClothingSC";
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> clothingsc = ProductHelper.ToMinDetailsList(ProductHandler.GetProductsBySubCategory(id));
            PagedList<MinDetails> model = new PagedList<MinDetails>(clothingsc, page, pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult Fragrance(int page = 1, int pageSize = 12)
        {
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> fragrance = ProductHelper.ToMinDetailsList(ProductHandler.GetProductsByCategory(3));
            PagedList<MinDetails> model = new PagedList<MinDetails>(fragrance, page, pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult FragranceSC(int id, int page = 1, int pageSize = 12)
        {
            TempData["id"] = id;
            ViewBag.SC_Name = "FragranceSC";
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> fragrancesc = ProductHelper.ToMinDetailsList(ProductHandler.GetProductsBySubCategory(id));
            PagedList<MinDetails> model = new PagedList<MinDetails>(fragrancesc, page, pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult Jewellery(int page = 1, int pageSize = 12)
        {
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> jewellery = ProductHelper.ToMinDetailsList(ProductHandler.GetProductsByCategory(5));
            PagedList<MinDetails> model = new PagedList<MinDetails>(jewellery, page, pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult JewellerySC(int id, int page = 1, int pageSize = 12)
        {
            TempData["id"] = id;
            ViewBag.SC_Name = "JewellerySC";
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> jewellerysc = ProductHelper.ToMinDetailsList(ProductHandler.GetProductsBySubCategory(id));
            PagedList<MinDetails> model = new PagedList<MinDetails>(jewellerysc, page, pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult Accessories(int page = 1, int pageSize = 12)
        {
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> accessories = ProductHelper.ToMinDetailsList(ProductHandler.GetProductsByCategory(6));
            PagedList<MinDetails> model = new PagedList<MinDetails>(accessories, page, pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult AccessoriesSC(int id, int page = 1, int pageSize = 12)
        {
            TempData["id"] = id;
            ViewBag.SC_Name = "AccessoriesSC";
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> accessoriessc = ProductHelper.ToMinDetailsList(ProductHandler.GetProductsBySubCategory(id));
            PagedList<MinDetails> model = new PagedList<MinDetails>(accessoriessc, page, pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult Others(int page = 1, int pageSize = 12)
        {
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> others = ProductHelper.ToMinDetailsList(ProductHandler.GetProductsByCategory(7));
            PagedList<MinDetails> model = new PagedList<MinDetails>(others, page, pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult OthersSC(int id, int page = 1, int pageSize = 12)
        {
            TempData["id"] = id;
            ViewBag.SC_Name = "OthersSC";
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> otherssc = ProductHelper.ToMinDetailsList(ProductHandler.GetProductsBySubCategory(id));
            PagedList<MinDetails> model = new PagedList<MinDetails>(otherssc, page, pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult ProductDetails(int id)
        {
            float sumofrating = 0;
            ViewBag.Reviews = new UserHandler().GetReviews(id);
            List<Review> listofreviews = new UserHandler().GetReviews(id);
            foreach (var item in listofreviews)
            {
                sumofrating += item.Rate;
            }
            ViewBag.ReviewCount = new UserHandler().ReviewCounter(id);
            ViewBag.Rating = (Int32)Math.Ceiling(sumofrating / ViewBag.ReviewCount);
            MaxDetails model = ProductHelper.ToMaxDetails(ProductHandler.GetProduct(id));
            int subcatid = model.SubCatId;
            ViewBag.Related = ProductHelper.ToMinDetailsList(ProductHandler.GetRelated(id, subcatid, 6));
            return View(model);
        }

        [HttpPost]
        public void ViewIncrement(int id)
        {
            using (DAL dal = new DAL())
            {
                Product product = ProductHandler.GetProductJust(id);
                int views = product.Views;
                views++;
                product.Views = views;
                dal.Entry(product).State = EntityState.Modified;
                dal.SaveChanges();
            }
        }

        [HttpGet]
        public ActionResult AddReview(int Id, string Username, string Userreview, string Userrating)
        {
            using (DAL dal = new DAL())
            {
                try
                {
                    Review review = new Review();

                    review.Name = Username;
                    review.CustomerReview = Userreview;
                    review.Rate = Convert.ToInt32(Userrating);
                    review.Status = false;
                    review.PostedDate = DateTime.Now.AddHours(5);
                    review.ProductId = Id;
                    dal.Reviews.Add(review);
                    dal.SaveChanges();
                    return Json("ok");
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }


        [HttpPost]
        public ActionResult Add(FormCollection data)
        {
            List<AvailableColor> availablecolors = ProductHandler.GetAvailableColors();
            try
            {
                Product p = new Product();
                string a = data["SecSubCat"];
                string[] temp = a.Split(',');
                int secondsubcategory = Convert.ToInt32(temp[0]);
                p.Name = data["Name"];
                p.MiniTitle = data["MiniTitle"];
                p.Description = data["Description"];
                p.Condition = data["Condition"];
                p.Price = Convert.ToSingle(data["Price"]);
                p.DiscountedPrice = Convert.ToSingle(data["DiscountedPrice"]);
                p.Weight = data["Weight"];
                p.Code = data["Code"];
                p.AdditionalDetails = data["AdditionalDetails"];
                p.Rating = 0;
                p.Views = 0;
                p.DisplayImage = "None";

                p.BestSellers = Convert.ToBoolean(data["BestSeller"].Split(',').First());
                p.Special = Convert.ToBoolean(data["Special"].Split(',').First());
                p.ShowCase = Convert.ToBoolean(data["ShowCase"].Split(',').First());
                p.IsFeatured = Convert.ToBoolean(data["IsFeatured"].Split(',').First());
                p.Availability = Convert.ToBoolean(data["Availability"].Split(',').First());
                p.Promotion = Convert.ToBoolean(data["Promotion"].Split(',').First());
                p.OnePiece = Convert.ToBoolean(data["OnePiece"].Split(',').First());
                p.TwoPiece = Convert.ToBoolean(data["TwoPiece"].Split(',').First());
                p.ThreePiece = Convert.ToBoolean(data["ThreePiece"].Split(',').First());
                p.SaleLabel = Convert.ToBoolean(data["SaleLabel"].Split(',').First());
                p.NewLabel = Convert.ToBoolean(data["NewLabel"].Split(',').First());
                p.IsActive = Convert.ToBoolean(data["IsActive"].Split(',').First());
                p.Fall = Convert.ToBoolean(data["Fall"].Split(',').First());
                p.Winter = Convert.ToBoolean(data["Winter"].Split(',').First());
                p.Spring = Convert.ToBoolean(data["Spring"].Split(',').First());
                p.Summer = Convert.ToBoolean(data["Summer"].Split(',').First());
                p.XS = Convert.ToBoolean(data["ExtraSmall"].Split(',').First());
                p.SM = Convert.ToBoolean(data["Small"].Split(',').First());
                p.MD = Convert.ToBoolean(data["Medium"].Split(',').First());
                p.LG = Convert.ToBoolean(data["Large"].Split(',').First());
                p.XL = Convert.ToBoolean(data["ExtraLarge"].Split(',').First());

                foreach (var c in availablecolors)
                {
                    if (Convert.ToBoolean(data[c.Name].Split(',').First()) == true)
                    {
                        p.Colors.Add(new Color { Name = c.Name, ColorCode = "hello" });
                    }
                    
                }

                p.CreatedDate = DateTime.Now.AddHours(5);
                p.Quantity = Convert.ToInt32(data["Quantity"]);
                p.SubCategory = new SubCategory { Id = Convert.ToInt32(data["SubCategory"]) };
                p.Wear = new Wear { Id = Convert.ToInt32(data["Fabric"]) };
                p.Brand = new Brand { Id = Convert.ToInt32(data["Brand"]) };
                p.Seller = new Models.User.Seller { Id = Convert.ToInt32(data["Seller"]) };
                p.SecondSubCategory = new SecondSubCategory { Id = Convert.ToInt32(data["SecSubCat"]) };

                long uno = DateTime.Now.Ticks;
                int counter = 0;
                foreach (string fcName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fcName];
                    if (!string.IsNullOrEmpty(file.FileName))
                    {
                        string url = "/Content/Images/Products/" + uno + "_" + ++counter + file.FileName.Substring(file.FileName.LastIndexOf('.'));
                        string path = Server.MapPath(url);
                        p.ProductImages.Add(new Image { Url = url, Priority = counter });
                        file.SaveAs(path);
                    }
                }
                ProductHandler.AddProduct(p);
            }
            catch (Exception ex)
            {
                throw;
            }

            return RedirectToAction("Add", "Products");
        }

        [HttpGet]
        public ActionResult SubCategories(int id)
        {
            DDLModel ddl = new DDLModel();
            ddl.Name = "SubCategory";
            ddl.Caption = "- Select SubCategory -";
            ViewBag.SubCategories = ProductHelper.ToSelectItemList(ProductHandler.GetSubCategoryByCategoryId(id));
            return PartialView("~/Views/Shared/_DropDownSubCategory.cshtml");
        }

        [HttpGet]
        public ActionResult SecondSubCategory(int id)
        {
            DDLModel ddl = new DDLModel();
            ddl.Name = "SecondSubCategory";
            ddl.Caption = "- Select SecondSubCategory -";
            ViewBag.SecondSubCatories = ProductHelper.ToSelectItemList(ProductHandler.GetFabricsByWearId(id));
            return PartialView("~/Views/Shared/_DropDownMenu.cshtml");
        }

        
        [HttpGet]
        public ActionResult GiveReview(int id)
        {
            TempData["ProductId"] = id;
            return PartialView("~/Views/Shared/_ModalReview.cshtml");
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            MaxDetails model = ProductHelper.ToMaxDetails(ProductHandler.GetProduct(id));
            return PartialView("~/Views/Shared/_ModalView.cshtml", model);
        }

        public ActionResult FPriceASC(int id, int page = 1, int pageSize = 12)
        {
            ViewBag.Name = "FPriceASC";
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> filtereditems = ProductHelper.ToMinDetailsList(ProductHandler.FPriceASC(id));
            PagedList<MinDetails> model = new PagedList<MinDetails>(filtereditems, page, pageSize);
            return View("~/Views/Products/" + TempData["Filter"] + ".cshtml", model);
        }

        public ActionResult FPriceDSC(int id, int page = 1, int pageSize = 12)
        {
            ViewBag.Name = "FPriceDSC";
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> filtereditems = ProductHelper.ToMinDetailsList(ProductHandler.FPriceDSC(id));
            PagedList<MinDetails> model = new PagedList<MinDetails>(filtereditems, page, pageSize);
            return View("~/Views/Products/" + TempData["Filter"] + ".cshtml", model);
        }

        public ActionResult FSCPriceASC(int id, int page = 1, int pageSize = 12)
        {
            SubCategory sc = ProductHandler.GetProductBySubCategoryId(id);
            ViewBag.SCName = sc.Name;
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> filtereditems = ProductHelper.ToMinDetailsList(ProductHandler.FSCPriceASC(id));
            PagedList<MinDetails> model = new PagedList<MinDetails>(filtereditems, page, pageSize);
            return View("~/Views/Products/" + TempData["Filter"] + ".cshtml", model);
        }

        public ActionResult FSCPriceDSC(int id, int page = 1, int pageSize = 12)
        {
            SubCategory sc = ProductHandler.GetProductBySubCategoryId(id);
            ViewBag.SCName = sc.Name;
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            List<MinDetails> filtereditems = ProductHelper.ToMinDetailsList(ProductHandler.FSCPriceDSC(id));
            PagedList<MinDetails> model = new PagedList<MinDetails>(filtereditems, page, pageSize);
            return View("~/Views/Products/" + TempData["Filter"] + ".cshtml", model);
        }
    }
}
