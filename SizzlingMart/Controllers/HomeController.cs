﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SizzlingMart.Models.Product;
using SizzlingMart.Models.ViewModel;


namespace SizzlingMart.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Clothing = ProductHelper.ToMinDetailsList(ProductHandler.GetLatestProductsByCategory(4, 1));
            ViewBag.MakeUp = ProductHelper.ToMinDetailsList(ProductHandler.GetLatestProductsByCategory(4, 2));
            ViewBag.Perfumes = ProductHelper.ToMinDetailsList(ProductHandler.GetLatestProductsByCategory(4, 3));
            ViewBag.SkinCare = ProductHelper.ToMinDetailsList(ProductHandler.GetLatestProductsByCategory(4, 4));
            ViewBag.Jewellery = ProductHelper.ToMinDetailsList(ProductHandler.GetLatestProductsByCategory(4, 5));
            ViewBag.Accessories = ProductHelper.ToMinDetailsList(ProductHandler.GetLatestProductsByCategory(4, 6));
            ViewBag.Others = ProductHelper.ToMinDetailsList(ProductHandler.GetLatestProductsByCategory(4, 7));
            //ViewBag.BestSellers = ProductHelper.ToMinDetailsList(ProductHandler.GetBestSellers(6));
            ViewBag.IsFeatured = ProductHelper.ToMinDetailsList(ProductHandler.GetIsFeatured(6));
            ViewBag.Special = ProductHelper.ToMinDetailsList(ProductHandler.GetSpecials(6));
            return View();
        }

        public ActionResult AboutUs()
        {
            return View();
        }

        public ActionResult ReturnPolicy()
        {
            return View();
        }

        public ActionResult StepsToOrder()
        {
            return View();
        }

        public ActionResult DeliveryInformation()
        {
            return View();
        }

        public ActionResult TermsConditions()
        {
            return View();
        }
    }
}