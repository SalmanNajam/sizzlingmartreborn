﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace SizzlingMart.Controllers
{
    public class SendMailController : Controller
    {
        // GET: SendMail
        public async Task<ActionResult> Index()
        {
            var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";
            var message = new MailMessage();
            message.To.Add(new MailAddress("sa1man@msn.com"));  // replace with valid value 
            message.From = new MailAddress("info@sizzlingmart.com");  // replace with valid value
            message.Subject = "You have been registered";
            message.Body = string.Format(body, "Salman Najam", "info@sizzlingmart.com", "Congratuations! You have been registered.");
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "info@sizzlingmart.com",  // replace with valid value
                    Password = "$Default123"  // replace with valid value
                };
                smtp.Credentials = credential;
                smtp.Host = "win03.tmd.cloud";
                smtp.Port = 25;
                smtp.UseDefaultCredentials = false;
                smtp.EnableSsl = false;
                await smtp.SendMailAsync(message);
                return RedirectToAction("Sent");
            }

        }
        
    }
}