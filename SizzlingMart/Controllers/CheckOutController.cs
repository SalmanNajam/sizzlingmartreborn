﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SizzlingMart.Controllers;
using SizzlingMart.Models.Order;
using SizzlingMart.Models.User;
using SizzlingMart.Models.ViewModel;

namespace SizzlingMart.Controllers
{
    public class CheckOutController : Controller
    {
        private int randomnumber(int min, int max)
        {
            Random rnum = new Random();
            return rnum.Next(min, max);
        }
        // GET: CheckOut
        public ActionResult Check()
        {
            DAL dal = new DAL();

            if (Session[WebUtil.CURRENT_USER] != null)
            {
                User current_user = (User)Session[WebUtil.CURRENT_USER];
                User user = (from u in dal.Users where u.Email == current_user.Email && current_user.IsActive == true select u).FirstOrDefault();
                string fullname = user.FirstName;
                string address = user.Address.StreetAddress;
                string phonenumber = user.DisplayPicture;
                if (address == null)
                {
                    TempData["ErrorMessage"] = "Please update your complete 'Shipping Details' to continue...";
                    return RedirectToAction("Dashboard", "Users");
                }
                else
                {
                    if (Session[WebUtil.CURRENT_USER] != null)
                    {
                        TempData.Keep("TotalItems");
                        TempData.Keep("Total");
                        CheckOutModel model = new CheckOutModel();
                        User usr = (User)Session[WebUtil.CURRENT_USER];

                        model.TotalItems = (int)TempData["TotalItems"];
                        model.OrderCode = randomnumber(11111111, 99999999).ToString();
                        model.Name = usr.FirstName +" "+ usr.LastName;
                        model.User_Id = usr.Id;
                        model.OrderDate = DateTime.Now.AddHours(5);
                        model.GrandTotal = Convert.ToDouble(TempData["Total"]);
                        model.Status = MyOrderStatus.OrderStatus.Pending;

                        foreach (CartItem item in (List<CartItem>)Session["cart"])
                        {
                            model.OrderDetails.Add(new OrderDetail
                            {
                                ProductId = item.addtocart.Id,
                                Name = item.addtocart.Name,
                                Image = item.addtocart.ImageUrl,
                                //item code is missing here 
                                Price = item.addtocart.Price,
                                Quantity = item.Quantity,
                                SubTotal = (item.addtocart.Price * item.Quantity)
                            });
                        }
                        return View("ConfirmOrder", model);
                    }
                }
            }
            else
            {
                TempData["error"] = "Please Login/Register to continue...";
                return RedirectToAction("Login", "Users");
            }
            return View();
        }

        public ActionResult Confirm()
        {
            double GrandTotal = Convert.ToDouble(TempData["Total"]);
            User usr = (User)Session[WebUtil.CURRENT_USER];
            string FullName = usr.FirstName +" "+ usr.LastName;
            int user_id = usr.Id;
            OrderInfo orderinfo = new OrderInfo();

            orderinfo.TotalItems = (int)TempData["TotalItems"];
            orderinfo.OrderCode = randomnumber(11111111, 99999999).ToString();
            orderinfo.Name = FullName;
            orderinfo.User_Id = user_id;
            orderinfo.OrderDate = DateTime.Now.AddHours(5);
            orderinfo.GrandTotal = GrandTotal;
            orderinfo.Status = MyOrderStatus.OrderStatus.Pending;

            foreach (CartItem item in (List<CartItem>)Session["cart"])
            {
                orderinfo.OrderDetails.Add(new OrderDetail
                {
                    ProductId = item.addtocart.Id,
                    Image = item.addtocart.ImageUrl,
                    Name = item.addtocart.Name,
                    Price = item.addtocart.Price,
                    Quantity = item.Quantity,
                    SubTotal = (item.addtocart.Price * item.Quantity)
                });
            }

            new OrdersHandler().AddOrder(orderinfo);

            List<CartItem> abc = (List<CartItem>)Session["cart"];
            abc.Clear();
            TempData["OrderConfirmationMessage"] = "Congratulations! Your order has been placed. You can also view the status in 'My Orders' tab. Thank you!";
            return RedirectToAction("Index", "Home");
        }
    }
}
