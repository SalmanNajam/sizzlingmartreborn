﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SizzlingMart.Models.ViewModel;

namespace SizzlingMart.Controllers
{
    public class CartItem
    {
        public int Quantity { get; set; }

        public int TotalPrice { get; set; }

        public AddToCartModel addtocart { get; set; }

        public CartItem(AddToCartModel a2c, int quantity, int totalprice)
        {
            addtocart = a2c;
            Quantity = quantity;
            TotalPrice = totalprice;
        }
    }
}