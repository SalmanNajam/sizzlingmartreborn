﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SizzlingMart.Models.ViewModel;
using SizzlingMart.Models.Product;

namespace SizzlingMart.Controllers
{
    public class ShoppingCartController : Controller
    {
        // GET: ShoppingCart
        public ActionResult Index()
        {
            return View();
        }

        private int isExist(int id, int Qty)
        {
            List<CartItem> cart = (List<CartItem>)Session["cart"];
            for (int i = 0; i < cart.Count; i++)
            {
                if (cart[i].addtocart.Id == id)
                {
                    foreach (var item in cart)
                    {
                        if (item.addtocart.Id == id)
                        {
                            item.Quantity += Qty;
                            item.TotalPrice += Convert.ToInt32(item.addtocart.Price);
                        }
                    }
                    return i;
                }
            }
            return -1;
        }

        private int isExisttoDelete(int id)
        {
            List<CartItem> cart = (List<CartItem>)Session["cart"];
            for (int i = 0; i < cart.Count; i++)
            {
                if (cart[i].addtocart.Id == id)
                {
                    foreach (var item in cart)
                    {
                        if (item.addtocart.Id == id)
                        {
                            item.Quantity += 1;
                            item.TotalPrice += Convert.ToInt32(item.addtocart.Price);
                        }
                    }
                    return i;
                }
            }
            return -1;
        }

        public int AddToCart(string Itemid, string ItemQuantity)
        {
            int index = 0;
            int pid = Convert.ToInt32(Itemid);
            int qty = Convert.ToInt32(ItemQuantity);
            AddToCartModel model = ProductHelper.ToAddToCartModel(ProductHandler.GetProductForCart(pid));

            if (Session["cart"] == null)
            {
                List<CartItem> cart = new List<CartItem>();
                cart.Add(new CartItem(model, qty, Convert.ToInt32(model.Price)));
                Session["cart"] = cart;
            }
            else
            {
                List<CartItem> cart = (List<CartItem>)Session["cart"];
                index = isExist(pid, qty);
                if (index == -1)
                {
                    cart.Add(new CartItem(model, qty, Convert.ToInt32(model.Price)));
                }
                else
                {
                    Session["cart"] = cart;
                }

            }

            List<CartItem> cart1 = (List<CartItem>)Session["cart"];

            int quantity = 0;
            int totalprice = 0;

            if (cart1 != null)
            {
                foreach (var item in cart1)
                {
                    quantity = quantity + item.Quantity;
                    totalprice = totalprice + item.TotalPrice;
                }
            }
            TempData["CountItems"] = quantity;
            TempData.Keep("CountItems");
            return quantity;
            //int number = totalprice;
            //string pricewithcomma = number.ToString("#,##0");
            //TempData["CountItems"] = quantity;
            //TempData["TotalPrice"] = pricewithcomma;
            //TempData.Keep("CountItems");
            //TempData.Keep("TotalPrice");
            //TempData["Cart"] = "<b>" + quantity + "</b> Item(s), <b>" + pricewithcomma + "</b>Rs";
            //TempData.Keep("Cart");
            //return "<b>" + quantity + "</b> Item(s), <b>" + pricewithcomma + "</b>Rs";

        }

        public ActionResult Delete(int id)
        {
            try
            {
                int index = isExisttoDelete(id);
                List<CartItem> cart = (List<CartItem>)Session["cart"];
                cart.RemoveAt(index);
                Session["cart"] = cart;
                int quantity = 0;
                if (cart != null)
                {
                    foreach (var item in cart)
                    {
                        quantity = quantity + item.Quantity;
                    }
                }
                TempData["CountItems"] = quantity;
                TempData.Keep("CountItems");

                return View("Cart");
            }
            catch (Exception)
            {
                TempData["Error"] = "There is some problem with the server. Please try again later!";
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpGet]
        public int CartCounter()
        {

            List<CartItem> cart1 = (List<CartItem>)Session["cart"];
            int quantity = 0;
            int totalprice = 0;

            if (cart1 != null)
            {
                foreach (var item in cart1)
                {
                    quantity = quantity + item.Quantity;
                    totalprice = totalprice + item.TotalPrice;
                }
            }
            TempData["CountItems"] = quantity;
            TempData.Keep("CountItems");
            return quantity;
            //int number = totalprice;
            //string pricewithcomma = number.ToString("#,##0");
            //TempData["CountItems"] = quantity;
            //TempData["TotalPrice"] = pricewithcomma;
            //TempData.Keep("CountItems");
            //TempData.Keep("TotalPrice");
            //return "<b>" + quantity + "</b> Item(s), <b>" + pricewithcomma + "</b>Rs";
            //return quantity;
        }

        public ActionResult ViewCart()
        {
            List<CartItem> cart1 = (List<CartItem>)Session["cart"];
            int quantity = 0;
            int totalprice = 0;

            if (cart1 != null)
            {
                foreach (var item in cart1)
                {
                    quantity = quantity + item.Quantity;
                    totalprice = totalprice + item.TotalPrice;
                }
            }
            TempData["CountItems"] = quantity;
            TempData.Keep("CountItems");
            return View("Cart");
        }

        [HttpPost]
        public ActionResult Update(FormCollection fc)
        {
            string[] quantities = fc.GetValues("quantity");
            List<CartItem> cart1 = (List<CartItem>)Session["cart"];
            for (int i = 0; i < cart1.Count; i++)
            {
                cart1[i].Quantity = Convert.ToInt32(quantities[i]);
            }
            Session["cart"] = cart1;
            int quantity = 0;
            int totalprice = 0;

            if (cart1 != null)
            {
                foreach (var item in cart1)
                {
                    quantity = quantity + item.Quantity;
                    totalprice = totalprice + item.TotalPrice;
                }
            }
            TempData["CountItems"] = quantity;
            TempData.Keep("CountItems");
            return View("Cart");
        }
    }
}