﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SizzlingMart.Models.User;
using SizzlingMart.Models.ViewModel;

namespace SizzlingMart.Controllers
{
    public class WishListController : Controller
    {

        public ActionResult Index(int id)
        {
            List<WishList> wishlistitemlist = UserHandler.GetWishlistItems(id);
            List<MinDetails> detailsofwishlistitems = UserHandler.ToMinDetailsList(wishlistitemlist);
            return View(detailsofwishlistitems);
        }

        public ActionResult CheckWishlist(int id)
        {
            if (Session[WebUtil.CURRENT_USER] != null)
            {
                User user = (User)Session[WebUtil.CURRENT_USER];
                int userid = user.Id;
                if (userid == id)
                {
                    List<WishList> wishlistitemlist = UserHandler.GetWishlistItems(id);
                    ViewBag.detailsofwishlistitems = UserHandler.ToMinDetailsList(wishlistitemlist);
                    return View("Wishlist");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }

            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private int isExisttoDelete(int id)
        {
            List<CartItem> cart = (List<CartItem>)Session["cart"];
            for (int i = 0; i < cart.Count; i++)
            {
                if (cart[i].addtocart.Id == id)
                {
                    foreach (var item in cart)
                    {
                        if (item.addtocart.Id == id)
                        {
                            item.Quantity += 1;
                            item.TotalPrice += Convert.ToInt32(item.addtocart.Price);
                        }
                    }
                    return i;
                }
            }
            return -1;
        }

        public ActionResult Delete(int id)
        {
            try
            {

                if (Session[WebUtil.CURRENT_USER] != null)
                {
                    User user = (User)Session[WebUtil.CURRENT_USER];
                    int userid = user.Id;
                    using (DAL dal = new DAL())
                    {
                        WishList wishlist = (from w in dal.WishLists where w.UserId == userid && w.ProductId == id select w).FirstOrDefault();
                        dal.WishLists.Remove(wishlist);
                        dal.SaveChanges();
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
                
                User user2 = (User)Session[WebUtil.CURRENT_USER];
                int userid2 = user2.Id;
                List<WishList> wishlistitemlist = UserHandler.GetWishlistItems(userid2);
                ViewBag.detailsofwishlistitems = UserHandler.ToMinDetailsList(wishlistitemlist);
                return View("Wishlist");
            }
            catch (Exception)
            {
                TempData["Error"] = "There is some problem with the server. Please try again later!";
                return RedirectToAction("MyAccount", "Users");
            }
        }


        [HttpPost]
        public int AddToWishlist(int PID, string UID)
        {
            int wlmessage;
            using (DAL dal = new DAL())
            {
                int pid = Convert.ToInt32(PID);
                int uid = Convert.ToInt32(UID);
                WishList wl = (from w in dal.WishLists where w.ProductId == pid && w.UserId == uid select w).FirstOrDefault();
                if (wl == null)
                {
                    WishList wishlist = new WishList();
                    wishlist.ProductId = pid;
                    wishlist.UserId = uid;
                    dal.WishLists.Add(wishlist);
                    dal.SaveChanges();
                    wlmessage = 1;
                }
                else
                {
                    wlmessage = 0;
                }
                return wlmessage;
            }
        }
    }
}