﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SizzlingMart
{
    public static class WebUtil
    {
        public const int AdminRole = 1;
        public const string CURRENT_USER = "User";
        public const string USER_COOKIE = "RememberMe";
    }
}