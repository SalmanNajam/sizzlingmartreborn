﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using SizzlingMart.Models.Product;
using SizzlingMart.Models.User;
using SizzlingMart.Models.Order;

namespace SizzlingMart
{
    public class DAL : DbContext
    {
        public DAL() : base("name=ConnString")
        {

        }

        //PRODUCT SECTION
        public DbSet<AvailableColor> AvailableColors { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<SecondSubCategory> SecondSubCategory { get; set; }
        public DbSet<Image> ProductImages { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Season> Seasons { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<Wear> Wears { get; set; }

        //USER SECTION
        public DbSet<Address> Addresses { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Seller> Sellers { get; set; }
        public DbSet<ShopImage> ShopImages { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<WishList> WishLists { get; set; }

        //ORDER SECTION
        public DbSet<OrderInfo> OrderInfoes { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }

    }
}