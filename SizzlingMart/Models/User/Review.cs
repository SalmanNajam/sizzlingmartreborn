﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SizzlingMart.Models.User
{
    public class Review
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CustomerReview { get; set; }
        public float Rate { get; set; }
        public DateTime PostedDate { get; set; }
        public bool Status { get; set; }
        public int ProductId { get; set; }
    }
}