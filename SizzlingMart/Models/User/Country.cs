﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SizzlingMart.Models.User
{
    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}