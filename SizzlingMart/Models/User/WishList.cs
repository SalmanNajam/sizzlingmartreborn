﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SizzlingMart.Models.Product;

namespace SizzlingMart.Models.User
{
    public class WishList
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int UserId { get; set; }
        public int Quantity { get; set; }
    }
}