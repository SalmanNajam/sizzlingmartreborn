﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SizzlingMart.Models.User
{
    public class Address
    {
        public int Id { get; set; }
        public string StreetAddress { get; set; }
        public virtual City City { get; set; }
    }
}