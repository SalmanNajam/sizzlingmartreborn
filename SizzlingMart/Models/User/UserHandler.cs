﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SizzlingMart.Models.Product;
using SizzlingMart.Models.ViewModel;

namespace SizzlingMart.Models.User
{
    public class UserHandler
    {
        public List<Country> GetCountries()
        {
            using (DAL dal = new DAL())
            {
                return (from c in dal.Countries select c).ToList();
            }
        }

        public int WishlistCount(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from w in dal.WishLists where w.UserId == id select w).Count();
            }
        }

        public static List<WishList> GetWishlistItems(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from w in dal.WishLists where w.UserId == id select w).ToList();
            }
        }

        public static MinDetails ToMinDetails(int id)
        {
            using (DAL dal = new DAL())
            {
                Product.Product product = (from p in dal.Products where p.Id == id select p).FirstOrDefault();
                MinDetails model = new MinDetails();
                model.Id = product.Id;
                model.Name = product.Name;
                model.MiniTitle = product.MiniTitle;
                model.Description = product.Description;
                model.Price = product.Price;
                model.DiscountedPrice = product.DiscountedPrice;
                model.SaleLabel = product.SaleLabel;
                if (product.ProductImages != null && product.ProductImages.Count >= 1)
                {
                    model.ImageUrl = product.ProductImages.First().Url;
                }
                return model;
            }
        }

        public static List<MinDetails> ToMinDetailsList(List<WishList> wishlist)
        {
            List<MinDetails> TempList = null;
            if (wishlist != null)
            {
                TempList = new List<MinDetails>();
                foreach (var m in wishlist)
                {
                    TempList.Add(ToMinDetails(m.ProductId));
                }
                TempList.TrimExcess();
            }
            return TempList;
        }

        public List<City> GetCities(Country country)
        {
            using (DAL dal = new DAL())
            {
                return (from c in dal.Cities where c.Country.Id == country.Id select c).ToList();
            }
        }

        //public List<Cart> ViewItems()
        //{
        //    using (DAL dal = new DAL())
        //    {
        //        return (from c in dal.Carts select c).ToList();
        //    }
        //}

        public int ReviewCounter(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from u in dal.Reviews where u.ProductId == id && u.Status == true select u).Count();
            }
        }

        public List<Review> GetReviews(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from u in dal.Reviews where u.ProductId == id && u.Status == true select u).ToList();
            }
        }

        public List<User> GetUsers()
        {
            using (DAL dal = new DAL())
            {
                return (from u in dal.Users
                        .Include("Role") //eager loading
                        .Include("Address.City.Country")
                        select u).ToList();
            }
        }

        public User GetUserById(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from u in dal.Users where u.Id == id select u).FirstOrDefault();
            }
        }

        public User GetUserByEmail(string email)
        {
            using (DAL dal = new DAL())
            {
                return (from u in dal.Users
                        .Include("Role") //eager loading
                        .Include("Address.City.Country")
                        where u.Email.Equals(email)
                        select u).FirstOrDefault();
            }
        }

        public User GetUser(string email, string password)
        {
            using (DAL dal = new DAL())
            {
                return (from u in dal.Users
                        .Include("Role") //eager loading
                        .Include("Address.City.Country")
                        where u.Email.Equals(email) && u.Password.Equals(password)
                        select u).FirstOrDefault();
            }
        }



        public List<Role> GetRoles()
        {
            using (DAL dal = new DAL())
            {
                return (from u in dal.Roles
                        select u).ToList();
            }
        }
    }
}