﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SizzlingMart.Models.User
{
    public class ShopImage
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int Priority { get; set; }
        public string Caption { get; set; }
        public int SellerId { get; set; }
    }
}