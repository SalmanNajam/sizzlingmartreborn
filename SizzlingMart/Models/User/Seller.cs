﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SizzlingMart.Models.User
{
    public class Seller
    {
        public int Id { get; set; }
        public string ShopName { get; set; }
        public string SellerName { get; set; }
        public string DisplayPicture { get; set; }
        public string Description { get; set; }
        public string Contact1 { get; set; }
        public string Contact2 { get; set; }
        public string Contact3 { get; set; }
        public string Timings { get; set; }
    }
}