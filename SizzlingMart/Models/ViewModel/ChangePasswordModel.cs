﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SizzlingMart.Models.ViewModel
{
    public class ChangePasswordModel
    {
        [Required(ErrorMessage = "Password Required")]
        [Display(Name = "Change Password")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Password should not be less then 6 characters")]
        public string ChangedPassword { get; set; }
    }
}