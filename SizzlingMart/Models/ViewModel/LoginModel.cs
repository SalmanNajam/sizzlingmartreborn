﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SizzlingMart.Models.ViewModel
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Email ID Required")]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email1 { get; set; }

        [Required(ErrorMessage = "Password Required")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password1 { get; set; }

        [Display(Name = "Remember Me")]
        public bool RememberMe1 { get; set; }
    }
}