﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SizzlingMart.Models.Order;

namespace SizzlingMart.Models.ViewModel
{
    public class CheckOutModel
    {
        public CheckOutModel()
        {
            OrderDetails = new List<OrderDetail>();
        }
        //public int Id { get; set; }

        public int TotalItems { get; set; }
        public string OrderCode { get; set; }
        public string Name { get; set; }
        public int User_Id { get; set; }
        public DateTime OrderDate { get; set; }
        public double GrandTotal { get; set; }
        public MyOrderStatus.OrderStatus Status { get; set; }
        public virtual List<OrderDetail> OrderDetails { get; set; }
    }
}