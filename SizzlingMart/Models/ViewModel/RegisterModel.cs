﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SizzlingMart.Models.ViewModel
{
    [Bind(Exclude = "Id")]

    public class RegisterModel
    {
        [Required(ErrorMessage = "First Name Required")]
        [Display(Name = "First Name")]
        [DataType(DataType.Text)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name Required")]
        [Display(Name = "First Name")]
        [DataType(DataType.Text)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email Required")]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Contact Number Required")]
        [Display(Name = "Contact")]
        [DataType(DataType.Text)]
        public string Contact { get; set; }

        [Required(ErrorMessage = "Address Required")]
        [Display(Name = "Address")]
        [DataType(DataType.Text)]
        public string Address { get; set; }

        [Required(ErrorMessage = "Select City")]
        [Display(Name = "City")]
        [DataType(DataType.Text)]
        public string City { get; set; }

        [Required(ErrorMessage = "Select Country")]
        [Display(Name = "Country")]
        [DataType(DataType.Text)]
        public string Country { get; set; }

        [Required(ErrorMessage = "Password Required")]
        [Display(Name = "RegPassword")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Password should not be less then 6 characters")]
        public string RegPassword { get; set; }

        [Display(Name = "PrivacyPolicy")]
        public bool PrivacyPolicy { get; set; }
    }
}