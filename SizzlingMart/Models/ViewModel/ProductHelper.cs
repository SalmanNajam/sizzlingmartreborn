﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SizzlingMart.Models;
using SizzlingMart.Models.ViewModel;
using System.Web.Mvc;

namespace SizzlingMart.Models.ViewModel
{
    public class ProductHelper
    {
        public static AddToCartModel ToAddToCartModel(Product.Product product)
        {
            AddToCartModel model = new AddToCartModel();
            model.Id = product.Id;
            model.Name = product.Name;
            model.Price = product.Price;
            model.Code = product.Code;
            if (product.ProductImages != null && product.ProductImages.Count >= 1)
            {
                model.ImageUrl = product.ProductImages.First().Url;
            }
            return model;
        }

        public static MinDetails ToMinDetails(Product.Product product)
        {
            MinDetails model = new MinDetails();
            model.Id = product.Id;
            model.Name = product.Name;
            model.MiniTitle = product.MiniTitle;
            model.Description = product.Description;
            model.Price = product.Price;
            model.DiscountedPrice = product.DiscountedPrice;
            model.SaleLabel = product.SaleLabel;
            if (product.ProductImages != null && product.ProductImages.Count >= 1)
            {
                model.ImageUrl = product.ProductImages.First().Url;
            }
            return model;
        }

        public static List<MinDetails> ToMinDetailsList(List<Product.Product> products)
        {
            List<MinDetails> TempList = null;
            if (products != null)
            {
                TempList = new List<MinDetails>();
                foreach (var m in products)
                {
                    TempList.Add(ToMinDetails(m));
                }
                TempList.TrimExcess();
            }
            return TempList;
        }

        public static List<SelectListItem> ToSelectItemList(dynamic values)
        {
            List<SelectListItem> templist = null;
            if (values != null)
            {
                templist = new List<SelectListItem>();
                foreach (var v in values)
                {
                    templist.Add(new SelectListItem { Text = v.Name, Value = Convert.ToString(v.Id) });
                }
                templist.TrimExcess();
            }
            return templist;
        }

        public static List<SelectListItem> ToSelectItemListForSellers(dynamic values)
        {
            List<SelectListItem> templist = null;
            if (values != null)
            {
                templist = new List<SelectListItem>();
                foreach (var v in values)
                {
                    templist.Add(new SelectListItem { Text = v.ShopName, Value = Convert.ToString(v.Id) });
                }
                templist.TrimExcess();
            }
            return templist;
        }

        public static MaxDetails ToMaxDetails(Product.Product product)
        {
            MaxDetails model = new MaxDetails();
            if (product != null)
            {
                model.Id = product.Id;
                model.Name = product.Name;
                model.MiniTitle = product.MiniTitle;
                model.Description = product.Description;
                model.Views = product.Views;
                model.Price = product.Price;
                model.DiscountedPrice = product.DiscountedPrice;
                model.SaleLabel = product.SaleLabel;
                model.Brand = product.Brand.Name;
                model.Seller = product.Seller.ShopName;
                model.Fabric = product.SecondSubCategory.Name;
                model.Wear = product.Wear.Name;
                model.Code = product.Code;
                model.AdditionalDetails = product.AdditionalDetails;
                model.SubCatId = product.SubCategory.Id;
                model.SubCategory = product.SubCategory.Name;
                model.Category = product.SubCategory.Category.Name;
                //model.Reviews = product.Reviews;
                model.Colors = product.Colors;
                if (product.ProductImages != null)
                {
                    foreach (var m in product.ProductImages)
                    {
                        model.ProductImages.Add(m.Url);
                    }
                }
            }
            return model;
        }
    }
}