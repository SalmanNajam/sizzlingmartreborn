﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SizzlingMart.Models.ViewModel
{
    public class AddToCartModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public float Price { get; set; }
        public string Code { get; set; }
        public string ImageUrl { get; set; }
        public int Quantity { get; set; }
        public float SubTotal { get; set; }
    }
}