﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SizzlingMart.Models.ViewModel
{
    public class MinDetails
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string MiniTitle { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public float DiscountedPrice { get; set; }
        public bool SaleLabel { get; set; }
        public string ImageUrl { get; set; }
        public int Rating { get; set; }
        public string category { get; set; }
        public string subcategory { get; set; }
    }
}