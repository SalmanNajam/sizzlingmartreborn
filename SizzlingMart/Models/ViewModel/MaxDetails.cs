﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SizzlingMart.Models.Product;
using SizzlingMart.Models.User;

namespace SizzlingMart.Models.ViewModel
{
    public class MaxDetails : MinDetails
    {
        public MaxDetails()
        {
            ProductImages = new List<string>();
            Reviews = new List<Review>();
            Colors = new List<Color>();
        }
        public int Views { get; set; }
        public string Brand { get; set; }
        public string Seller { get; set; }
        public string Fabric { get; set; }
        public string Code { get; set; }
        public string AdditionalDetails { get; set; }
        public int SubCatId { get; set; }
        public string SubCategory { get; set; }
        public string Category { get; set; }
        public string Wear { get; set; }
        public List<string> ProductImages { get; set; }
        public List<Review> Reviews { get; set; }
        public List<Color> Colors { get; set; }
    }
}