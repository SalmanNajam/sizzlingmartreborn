﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SizzlingMart.Models.Product
{
    public class AvailableColor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}