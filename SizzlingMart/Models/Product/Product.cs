﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SizzlingMart.Models.User;

namespace SizzlingMart.Models.Product
{
    public class Product
    {
        private Product product;

        public Product()
        {
            ProductImages = new List<Image>();
            Colors = new List<Color>();
        }

        public Product(Product product)
        {
            this.product = product;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string MiniTitle { get; set; }
        public string Description { get; set; }
        public string Condition { get; set; }
        public string DisplayImage { get; set; }
        public float Price { get; set; }
        public float DiscountedPrice { get; set; }
        public string Weight { get; set; }
        public int Rating { get; set; }
        public string Code { get; set; }
        public string AdditionalDetails { get; set; }
        public bool BestSellers { get; set; }
        public bool Special { get; set; }
        public bool ShowCase { get; set; }
        public bool IsFeatured { get; set; }
        public bool Availability { get; set; }
        public bool Promotion { get; set; }
        public bool OnePiece { get; set; }
        public bool TwoPiece { get; set; }
        public bool ThreePiece { get; set; }
        public bool SaleLabel { get; set; }
        public bool NewLabel { get; set; }
        public bool IsActive { get; set; }
        public bool Fall { get; set; }
        public bool Winter { get; set; }
        public bool Spring { get; set; }
        public bool Summer { get; set; }
        public bool XS { get; set; }
        public bool SM { get; set; }
        public bool MD { get; set; }
        public bool LG { get; set; }
        public bool XL { get; set; }
        public int? Quantity { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Views { get; set; }
        public virtual Brand Brand { get; set; }
        public virtual SecondSubCategory SecondSubCategory { get; set; }
        public virtual Seller Seller { get; set; }
        public virtual SubCategory SubCategory { get; set; }
        public virtual List<Image> ProductImages { get; set; }
        public virtual List<Color> Colors { get; set; }
        public virtual Wear Wear { get; set; }
    }
}