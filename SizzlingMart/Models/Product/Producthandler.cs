﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SizzlingMart.Models.Product;
using SizzlingMart.Models.User;
using System.Data.Entity;

namespace SizzlingMart.Models.Product
{
    public class ProductHandler
    {

        public static int GetRating(int id)
        {

            float sumofrating = 0;
            int rating = 0;
            int count;
            List<Review> listofreviews = new UserHandler().GetReviews(id);
            foreach (var item in listofreviews)
            {
                sumofrating += item.Rate;
            }
            count = new UserHandler().ReviewCounter(id);
            rating = (Int32)Math.Ceiling(sumofrating / count);
            if (rating >= 0)
            {
                return rating;
            }
            else
            {
                return rating = 0;
            }
            
        }

        public static List<Product> GetProductsBySeller(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                        .Include("ProductImages")
                        where p.Seller.Id == id
                        select p).ToList();
            }
        }

        public static List<Product> GetSpecials(int counter)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                        .Include("ProductImages")
                        where p.Special == true
                       select p).OrderBy(x => Guid.NewGuid()).Take(counter).ToList();
            }
        }
    

        public static Product GetProductJust(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                        where p.Id == id
                        select p).FirstOrDefault();
            }
        }
        public static Product GetProduct(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                        .Include("Brand")
                        .Include("SecondSubCategory")
                        .Include("Seller")
                        .Include("Colors")
                        .Include("Subcategory.category")
                        .Include("ProductImages")
                        .Include("Wear")
                        where p.Id == id
                        select p).FirstOrDefault();
            }
        }

        public static Product GetProductForCart(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                        //.Include("Reviews")
                        //.Include("Brand")
                        //.Include("Fabric")
                        //.Include("Seller")
                        //.Include("Colors")
                        //.Include("Subcategory.category")
                        .Include("ProductImages")
                        where p.Id == id
                        select p).FirstOrDefault();
            }
        }

        public static Product GetProductForReview(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                        .Include("Reviews")
                        .Include("ProductImages")
                        .Include("Colors")
                        where p.Id == id
                        select p).FirstOrDefault();
            }
        }

        public static List<Product> GetProducts()
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                        //.Include("Review")
                        //.Include("Brand")
                        //.Include("Fabric")
                        //.Include("Seller")
                        //.Include("Subcategory.category")
                        //.Include("ProductImages")
                        select p).ToList();
            }
        }

        public static List<Product> GetProductsByPriceASC(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                            .Include("Subcategory.category")
                            .Include("ProductImages")
                        where p.SubCategory.Category.Id == id
                        orderby p.Price ascending
                        select p).ToList();
            }
        }

        public static List<Category> GetCategories()
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Categories select p).ToList();
            }
        }

        public static List<Country> GetCountries()
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Countries select p).ToList();
            }
        }

        public static List<City> GetCities()
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Cities select p).ToList();
            }
        }

        public static List<AvailableColor> GetAvailableColors()
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.AvailableColors select p).ToList();
            }
        }

        public static List<Wear> GetWears()
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Wears select p).ToList();
            }
        }

        public static List<Brand> GetBrands()
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Brands select p).ToList();
            }
        }

        public static List<Seller> GetSellers()
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Sellers select p).ToList();
            }
        }

        public static void AddProduct(Product product)
        {
            using (DAL dal = new DAL())
            {
                dal.Entry(product.Brand).State = EntityState.Unchanged;
                dal.Entry(product.SecondSubCategory).State = EntityState.Unchanged;
                dal.Entry(product.Seller).State = EntityState.Unchanged;
                dal.Entry(product.SubCategory).State = EntityState.Unchanged;
                dal.Entry(product.Wear).State = EntityState.Unchanged;
                dal.Products.Add(product);
                dal.SaveChanges();
            }
        }

        public static void DeleteProduct(Product product)
        {
            using (DAL dal = new DAL())
            {
                Product toDelete = (from p in dal.Products
                        //.Include("Review")
                        //.Include("Brand")
                        //.Include("Fabric")
                        //.Include("Seller")
                        //.Include("Subcategory.category")
                        //.Include("ProductImages")
                        where p.Id == product.Id
                        select p).FirstOrDefault();
                dal.Products.Remove(toDelete);
                dal.SaveChanges();
            }
        }

        public static List<Product> GetLatestProducts(int counter)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                        //.Include("Review")
                        //.Include("Brand")
                        //.Include("Fabric")
                        //.Include("Seller")
                        .Include("Subcategory.category")
                        .Include("ProductImages")
                        where p.IsActive == true && p.ShowCase == true
                        orderby p.CreatedDate descending
                        select p).Take(counter).ToList();
            }
        }

        public static List<Product> GetLatestProductsByCategory(int counter, int categoryid)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                        //.Include("Review")
                        //.Include("Brand")
                        //.Include("Fabric")
                        //.Include("Seller")
                        .Include("Subcategory.category")
                        .Include("ProductImages")
                        where p.IsActive == true && p.ShowCase == true && p.SubCategory.Category.Id == categoryid
                        orderby p.CreatedDate descending
                        select p).OrderBy(x => Guid.NewGuid()).Take(counter).ToList();
            }
        }

        public static List<Product> GetProductsByCategory(int categoryid)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                        //.Include("Review")
                        //.Include("Brand")
                        //.Include("Fabric")
                        //.Include("Seller")
                        .Include("Subcategory.category")
                        .Include("ProductImages")
                        where p.IsActive == true && p.SubCategory.Category.Id == categoryid
                        orderby p.CreatedDate descending
                        select p).ToList();
            }
        }

        public static List<Product> GetProductsBySubCategory(int subcategoryid)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                        //.Include("Review")
                        //.Include("Brand")
                        //.Include("Fabric")
                        //.Include("Seller")
                        .Include("Subcategory.category")
                        .Include("ProductImages")
                        where p.IsActive == true && p.SecondSubCategory.Id == subcategoryid
                        orderby p.CreatedDate descending
                        select p).ToList();
            }
        }

        public static List<Product> GetProductsBySubCategoryOriginal(int subcategoryid)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                        //.Include("Review")
                        //.Include("Brand")
                        //.Include("Fabric")
                        //.Include("Seller")
                        .Include("Subcategory.category")
                        .Include("ProductImages")
                        where p.IsActive == true && p.SubCategory.Id == subcategoryid
                        orderby p.CreatedDate descending
                        select p).ToList();
            }
        }

        public static List<Product> GetBestSellers(int counter)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                        //.Include("Review")
                        //.Include("Brand")
                        //.Include("Fabric")
                        //.Include("Seller")
                        //.Include("Subcategory.category")
                        .Include("ProductImages")
                        where p.IsActive == true && p.ShowCase == true && p.BestSellers == true
                        orderby p.CreatedDate descending
                        select p).OrderBy(x => Guid.NewGuid()).Take(counter).ToList();
            }
        }

        public static List<Product> GetIsFeatured(int counter)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                        //.Include("Review")
                        //.Include("Brand")
                        //.Include("Fabric")
                        //.Include("Seller")
                        //.Include("Subcategory.category")
                        .Include("ProductImages")
                        where p.IsActive == true && p.ShowCase == true && p.IsFeatured == true
                        orderby p.CreatedDate descending
                        select p).OrderBy(x => Guid.NewGuid()).Take(counter).ToList();
            }
        }


        public List<Product> GetProductsByBrand(Brand brand)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                        where p.Brand.Id == brand.Id
                        select p).ToList();

            }
        }

        public static List<SubCategory> GetSubCategoryByCategoryId(int categoryid)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.SubCategories
                        where p.Category.Id == categoryid
                        orderby p.Name ascending
                        select p).ToList();
            }
        }

        public static List<Product> GetProductsByCategoryId(int categoryid)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                        .Include("Subcategory.category")
                        .Include("ProductImages")
                        where p.SubCategory.Category.Id == categoryid
                        select p).ToList();
            }
        }

        public static List<Product> GetRelated(int id, int subcategory, int counter)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                        .Include("Subcategory.category")
                        .Include("ProductImages")
                        where p.Id != id && p.SubCategory.Id == subcategory
                        orderby p.CreatedDate descending
                        select p).OrderBy(x => Guid.NewGuid()).Take(counter).ToList();
            }
        }

        public static List<SecondSubCategory> GetFabricsByWearId(int wearid)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.SecondSubCategory
                        where p.SubCategory.Id == wearid
                        orderby p.Name ascending
                        select p).ToList();
            }
        }

        public static List<Product> FPriceASC(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                 .Include("ProductImages")
                        orderby p.Price ascending
                        where p.SecondSubCategory.SubCategory.Category.Id == id
                        select p).ToList();
            }
        }

        public static List<Product> FPriceDSC(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                 .Include("ProductImages")
                        orderby p.Price descending
                        where p.SecondSubCategory.SubCategory.Category.Id == id
                        select p).ToList();
            }
        }

        public static List<Product> FSCPriceASC(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                 .Include("ProductImages")
                        orderby p.Price ascending
                        where p.SecondSubCategory.Id == id
                        select p).ToList();
            }
        }

        public static List<Product> FSCPriceDSC(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.Products
                 .Include("ProductImages")
                        orderby p.Price descending
                        where p.SecondSubCategory.Id == id
                        select p).ToList();
            }
        }

        public static SubCategory GetProductBySubCategoryId(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from p in dal.SubCategories
                        where p.Id == id
                        select p).FirstOrDefault();
            }
        }
    }
}