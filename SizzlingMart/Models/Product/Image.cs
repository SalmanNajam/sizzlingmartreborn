﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SizzlingMart.Models.Product
{
    public class Image
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int Priority { get; set; }
        public string Caption { get; set; }
    }
}