﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SizzlingMart.Models.Order
{
    public class OrderDetail
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public double SubTotal { get; set; }
    }
}