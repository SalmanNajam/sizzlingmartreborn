﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SizzlingMart.Models.Order;
using SizzlingMart;

namespace SizzlingMart.Models.Order
{
    public class OrdersHandler
    {
        public void AddOrder(OrderInfo orderinfo)
        {
            using (DAL dal = new DAL())
            {
                dal.OrderInfoes.Add(orderinfo);
                dal.SaveChanges();
            }
        }

        public OrderInfo GetOrder(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from o in dal.OrderInfoes where o.Id == id select o).FirstOrDefault();
            }
        }

        public void SaveStatus(OrderInfo orderinfo)
        {
            using (DAL dal = new DAL())
            {
                dal.SaveChanges();
            }
        }

        public List<OrderInfo> MyOrders(int id)
        {
            using (DAL dal = new DAL())
            {
                return (from o in dal.OrderInfoes
                        .Include("OrderDetails")
                        where o.User_Id == id
                        select o).ToList();
            }
        }

        public List<OrderInfo> PendingOrders()
        {
            using (DAL dal = new DAL())
            {
                return (from o in dal.OrderInfoes where o.Status == MyOrderStatus.OrderStatus.Pending select o).ToList();
            }
        }

        public List<OrderInfo> InProcessOrders()
        {
            using (DAL dal = new DAL())
            {
                return (from o in dal.OrderInfoes where o.Status == MyOrderStatus.OrderStatus.InProcess select o).ToList();
            }
        }

        public List<OrderInfo> DeliveredOrders()
        {
            using (DAL dal = new DAL())
            {
                return (from o in dal.OrderInfoes where o.Status == MyOrderStatus.OrderStatus.Delivered select o).ToList();
            }
        }

        public List<OrderInfo> CancelledOrders()
        {
            using (DAL dal = new DAL())
            {
                return (from o in dal.OrderInfoes where o.Status == MyOrderStatus.OrderStatus.Cancelled select o).ToList();
            }
        }
    }
}
