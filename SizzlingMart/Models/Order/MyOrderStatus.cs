﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SizzlingMart.Models.Order
{
    public class MyOrderStatus
    {
        public OrderStatus Orders { get; set; }

        public enum OrderStatus
        {
            SelectStatus,
            Pending,
            InProcess,
            Delivered,
            Cancelled
        }
    }
}